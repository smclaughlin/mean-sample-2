var MongoClient = require('mongodb').MongoClient;
var Promise = require("bluebird");
var url = '';
var mongoskin = require('mongoskin')
var getOrdersByCompany;

var uri, collectionName;

uri = 'mongodb://sample:sample@dharma.mongohq.com:10073/newsriver-sandbox';
collectionName = "sample";

var module = {};

var db = mongoskin.db(uri, {safe:true})
var collection = db.collection(collectionName);

exports.getOrdersByCompany = function(companyName){
  return new Promise(function(resolve, reject){
    collection.find({"CompanyName": companyName}).toArray(function(err, docs) {
        if(err){
          return reject(err);
        }
      resolve(docs);
    });      
  });
}

exports.getOrderSummary = function(){
  return new Promise(function(resolve, reject){
    collection.aggregate([{$group:{ _id: "$Item", count: {$sum: 1}}}, {$sort: {"count": -1}}],
      function(err, summary){
        if(err){
          return reject(err);
        }
        resolve(summary);
      });
  });
}

exports.deleteOrder = function(id){
  return new Promise(function(resolve, reject){

    collection.remove({"Id": id}, function(err, result) {
      if(err){
        return reject(err);
      }
      resolve(result);
    });    
  });
}

exports.getOrdersByAddress = function(address){
  return new Promise(function(resolve, reject){
    collection.find({"Address": address})
      .toArray(function(err, result) {
        if(err){
          return reject(err);
        }
        resolve(result);
    }); 
  });   
};

exports.getAllOrders = function(){
  return new Promise(function(resolve, reject){
    collection.find({})
      .toArray(function(err, result) {
        if(err){
          return reject(err);
        }
        resolve(result);
      });
  })
}




