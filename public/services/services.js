app.factory('Orders', ['$http', function($http){
  
  var getOrdersByAddress = function (address){
    console.log("service called");
    return $http.get('/api/orders/company/address/' + address);
  };

  var getOrdersByCompany = function (name) {
    return $http.get('/api/orders/company/name/' + name)
  }

  var deleteOrder = function(id){
    return $http.delete('/api/orders/'+ id);
  }

  var getSummary = function(){
    return $http.get('/api/orders/summary')
  }

  var getAllOrders = function(){
    return $http.get('/api/orders');
  }

  return {
            getOrdersByAddress: getOrdersByAddress,
            getOrdersByCompany: getOrdersByCompany,
            deleteOrder: deleteOrder,
            getSummary: getSummary,
            getAllOrders: getAllOrders
        };
}])