app.controller('ordersController', 
  ['Orders', '$scope', '$sce', '$stateParams', '$location',
  function(Orders, $scope, $sce, $stateParams, $location){
  

  $scope.selectOptions = [{
                            filter: $sce.trustAsHtml("Company Name"), 
                            value:$sce.trustAsHtml("Name")
                          }, {
                            filter: $sce.trustAsHtml("Company Address"), 
                            value:$sce.trustAsHtml("Address")
                          }];

  
  
  $scope.filterClickHandler = function(field, value){
    if(field && value){
      $location.url("/dashboard?field=" + field + "&value=" + value);
    }
  }

  var filterHandler = function(filter, filterText){
    filter = filter.toString();
    if(filter == "address"){
      Orders.getOrdersByAddress(filterText).success(function(data){
        $scope.orders = data;
      })
    } else if(filter == "name"){
      Orders.getOrdersByCompany(filterText).success(function(data){
         $scope.orders = data;
      })
    }
  };

  $scope.deleteHandler = function(id){
    id = +id;
    Orders.deleteOrder(id).success(function(data){
      Orders.getAllOrders()
        .success(function(orders){
          $scope.orders = orders;
        })
    });
  }

  

  if($stateParams.field && $stateParams.value){
    var field = $stateParams.field.toLowerCase(), 
        value =  $stateParams.value;

    filterHandler(field, value);
  } else {
    Orders.getAllOrders()
      .success(function(data){
        $scope.orders = data;
      })
  }
}])