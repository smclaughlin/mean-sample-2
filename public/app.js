app.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.when('', '/dashboard');
  
  $stateProvider
    .state('home', {
      url: '/dashboard?field&value',
      template: '<order-table></order-table>',
      controller: 'ordersController'
    })
    .state('summary', {
      resolve : {
        'orderSummary' : function(Orders){
          return Orders.getSummary();
        }
      },
      url: '/summary',
      template: '<order-summary></order-summary>',
      controller: 'summaryController'
    })
});