
/**
 * Module dependencies.
 */

var express = require('express');
var http = require('http');
var path = require('path');

var orderDao = require('./lib/db.js');

var app = express();

var uri = 'mongodb://sample:sample@dharma.mongohq.com:10073/newsriver-sandbox';

// all environments
app.set('port', process.env.PORT || 3000);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.static(path.join(__dirname, 'public')));


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/api/orders/company/name/:name', function(req, res){
  orderDao.getOrdersByCompany(req.params.name)
    .then(function(docs){
      res.send(docs);
    })
    .catch(function(e){
      res.send(500);
    });
});

app.get('/api/orders/summary', function(req, res){
  orderDao.getOrderSummary()
    .then(function(summary){
      res.send(summary)
    });
});

app.del('/api/orders/:id', function(req, res){
  var id = +req.params.id
  orderDao.deleteOrder(id)
    .then(function(result){
      res.send(200)
    })
    .catch(function(e){
      res.send(500);
    });
});

app.get('/api/orders/company/address/:address', function(req, res){
  orderDao.getOrdersByAddress(req.params.address)
    .then(function(docs){
      res.send(docs);
    })
    .catch(function(e){
      res.send(500);
    });
});

app.get('/api/orders', function(req, res){
  orderDao.getAllOrders()
    .then(function(docs){
      res.send(docs);
    })
    .catch(function(e){
      res.send(500);
    });
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
